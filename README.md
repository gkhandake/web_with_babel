# Web with Babel

Exploring and experimenting with Babel/Babeljs transpliation with differnt ES6 helpers

# Install

<code>npm install</code>

# Build
```sh
$ grunt build-dist
```

# Server
```sh
$ node app/server.js
```

# Client

Click <a href="http://localhost:8080/views/es6.html">ES6</a> to open it in default browser or copy paste following into browser of your choice 'http://localhost:8080/views/es6.html'
