module.exports = function (api) {
    api.cache(true);

    const presets = [
        ['@babel/env', {
            useBuiltIns: 'entry',
            corejs: 3,
            targets: {
                edge: "17",
                firefox: "60",
                chrome: "67",
                safari: "11.1",
                ie: "11"
            }
        }],
        ['minify']
    ];
    const plugins = [];

    return {
        presets,
        plugins
    };
}