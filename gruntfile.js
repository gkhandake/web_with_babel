module.exports = function (grunt) {

    //Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        copy: {
            views: {
                expand: true,
                cwd: 'src/',
                src: 'views/**',
                dest: 'app/'
            },
            node: {
                expand: true,
                cwd: 'src/',
                src: './server.js',
                dest: 'app/'
            },
            babel: {
                expand: true,
                cwd: 'node_modules/@babel/polyfill/dist/',
                src: 'polyfill.js',
                dest: 'app/js/'
            }
        },
        babel: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'app/js/es6.min.js': 'src/scripts/es6.js'
                }
            }
        },
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Resister tasks
    grunt.registerTask('babel-dist', ['babel:dist']);
    grunt.registerTask('copy-dist', ['copy:views', 'copy:node', 'copy:babel']);
    grunt.registerTask('build-dist', ['babel:dist', 'copy-dist']);
}