try {

    console.log("Welcome to es6!");

    let a = ['a', 'b', 'c'];
    console.log("a : ", a);

    let b = [...a, 'foo'];
    console.log("b : ", b);

    let c = [...a]
    console.log("c : ", c);

    console.log("Iterating over c:");
    for (let i of c) {
        console.log("i =", i)
    }

    let d = [...new Set([1, 2, 3])];
    console.log("d : ", d);

    let e = [{ "one": 1 }, { "two": 2 }, { "three": 3 }]
    console.log("Iterating over e:");
    for (let i of e) {
        console.log(i);
    }

    const o = {
        Name: "Bob",
        Age: 25,
        Childern: [
            { Name: "Alice", Age: 5 },
            { Name: "Alex", Age: 2 }
        ]
    }

    function _allChildern(person) {
        let _childern = [];
        if (person && person.Age !== 0) {
            person.Childern = person.Childern.map((child) => {
                let _child = child;
                _child.School = 'home';
                return _child;
            });
            for (let item of person.Childern) {
                _childern.push({ Name: item.Name, Id: item.Age });
            }
        }
        return _childern;
    }

    console.log("o : ", o);
    console.log("allChildern : ", _allChildern(o));

    console.log(" Iterating 'foo' : ");
    for (let i of 'foo') {
        console.log(i);
    }

} catch (err) {
    console.error("catch : ", err);
}